\name{norm}
\alias{norm}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{L2 norm of a vector.
%%  ~~function to do ... ~~
}
\description{
Computes the L2 norm of a vector.
}
\usage{
norm(x)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{x}{
 A numeric vector.
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
  A number.
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{

}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }% use one of  RShowDoc("KEYWORDS")
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
