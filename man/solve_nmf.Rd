\name{solve_nmf}
\alias{solve_nmf}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{Compute a non-negative matrix factorization.
%%  ~~function to do ... ~~
}
\description{
Computes a non-negative matrix factorization using the anchor words paradigm. Anchor words are a set of representative words that ``tie down'' the topics and allow NMF to be cast as a convex program. Anchors are chosen using a QR-based algorithm and NMF is fit using non-negative least squares. See [thesis] for details and references to the relevant literature. Speed-ups by random projection are possible if desired, which can improve runtime by orders of magnitude.
}
\usage{
solve_nmf(input, project = FALSE, proj_dim)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{input}{
  An object of class nmf_input.
}
  \item{project}{
  A logical value. Perform random projections? A Guassian JL-type projection is performed for anchor   word selection while a Haddamard-type projection is performed for non-negative least squears. If     true, the following argument must be passed as well. Default is false.
}
  \item{proj_dim}{
    A 2-dimensional numerical vector giving the projection dimensions (one for the anchor algorithm      and one for non-negative least squares).
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
  An object of class nmf_output, containing the NMF factors (phi and theta), the anchor words, and
  elements of the corresponding nmf_input object (vocabulary, topics, groups).
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{

}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }% use one of  RShowDoc("KEYWORDS")
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
