\name{create_input}
\alias{create_input}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{Create an object of class nmf_input.}
\description{
  This function initializes a list containting all the required objects for downstream
  NMF computations: the term-document matrix, vocabulary, desired number of topics, and
  document groups (if applicable).
}
\usage{
create_input(tdm, vocab, groups, topics)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{tdm}{
  A term-document matrix to be supplied by the user. Note that this is a standard R numerical matrix,   not a TDM as defined by some other topic modelling packages.
}
  \item{vocab}{
  A character vector with all unique words in the corpus. Order should match that of the TDM's rows.
}
  \item{groups}{
  A character vector or factor containing groups to which the documents belong. These may be authors,   document sources, or some other discrete covariate.
}
\item{topics}{
  The desired number of topics to be fit.
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
  An object of class nmf_input.
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{

}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }% use one of  RShowDoc("KEYWORDS")
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
