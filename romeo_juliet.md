Introduction
------------

This document will serve as a brief introduction to the `inferNMF`
package, which smashes together a host of ideas surrounding non-negative
matrix factorization (NMF) and non-parametric inference. The point is to
solve some problems that are typically addressed using complicated
probabilistic graphical models. There’s nothing wrong with graphical
models; in fact, they’re quite cool! The issue is that fitting them can
be a major pain; entire careers have been built around inference
algorithms for graphical models. Despite the rapid progress in this
field – think HMC and its implementation in the wonderful Stan package –
certain model classes have yet to be adorned with inference schemes
whose speed, ease of use, and statistical accuracy are fully convincing.
For this reason we take a different tack. The basic idea is to slap
permutation tests and the boostrap on top of some recent developments in
NMF research. Motivation is provided by topic models in which there is
some sort of discrete covariate over the documents – we might partition
news stories by their source or a collection of books by author, for
instance. This is the setting we’ll stick to in what follows, but know
that `inferNMF` can be used for good old NMF, whatever your application
domain might be. The package’s langauge is unabashedly slanted towards
topic models, but you can simply ignore that and get on with factoring
matrices if you wish; for example, a “term-document matrix” could really
be any non-negative data matrix. Please refer to \[link to thesis when
available\] for further details on the motivation and underlying
mathematics, as well as references to the literature.

1. Building a Corpus
--------------------

We want to do topic modelling, so we’ll need some documents. Let’s use
Romeo and Juliet (RJ), freely available on Project Gutenberg:
<a href="https://www.gutenberg.org/ebooks/1112" class="uri">https://www.gutenberg.org/ebooks/1112</a>.
Lines in the play will be considered documents, and we’ll partition them
by the act in which they appear. Since acts are deliberately chosen to
divide a play’s plot into coherent segments, we should be able to detect
statistical variation in Shakespeare’s language as the play progresses.
I’ve prepared a term-document matrix (TDM) and a factor containing the
play’s acts using an admittedly haphazard script contained in this
repository – it’s not the prettiest but it gets the job done. Let’s load
the library and the RJ data:

    library(inferNMF)

    ## Loading required package: nnls

    ## Loading required package: phangorn

    ## Loading required package: ape

    ## 
    ## Attaching package: 'inferNMF'

    ## The following object is masked from 'package:base':
    ## 
    ##     norm

    data(tdm)
    data(words)
    data(acts)

2. Initializing an Object for NMF
---------------------------------

The first step is to build an object that can be passed to the NMF
solver. Rather than the solver requiring a slew of arguments, we instead
create an object of class `nmf_input` which carries around the necesarry
pieces, specified once and for all by the user. Most importantly, the
user must create and pass a term-document matrix, which is a good ol’ R
matrix – not a term-document matrix as created using some other topic
modelling packages. You’re on your own here – we provide no
functionality for creating a TDM or any of the other pre-processing that
topic modelling usually entails. Also of note: we really mean TDM, and
not DTM! Make sure the words are rows, otherwise everything will come
out backwards!

Once you’ve got your TDM, you need some discrete covariate structure
over the documents – a partitioning, as we’ll refer to it. These are the
acts of RJ in our example. The first 217 documents (which recall are
lines in the play) belong to act one, for instance. This is encoded as a
factor of equal length to the number of documents, with the entries
giving the value of the covariate like so:

    head(acts)

    ## [1] one one one one one one
    ## Levels: five four one three two

Finally, we’ll need a character vector which contains all the unique
words in the corpus. Crucially, they have to be in the same order as
that of the TDM’s rows. Again, you’re on your own here – create this
vector however you see fit. Here’s the first 25 words in our RJ courpus:

    head(words, n = 25)

    ##  [1] "abate"       "abbey"       "abed"        "abhorred"    "abhors"     
    ##  [6] "aboard"      "abound"      "abroach"     "absolv"      "absolver"   
    ## [11] "abus"        "abuse"       "abuses"      "accent"      "access"     
    ## [16] "accident"    "accidents"   "account"     "accurs"      "accustom"   
    ## [21] "ache"        "aches"       "aching"      "acknowledge" "acquaint"

That’s it! Now simply run

    my_input = create_input(tdm, words, acts, topics = 25)

which creates an object `my_input` of class `nmf_input` (this is
basically just a named list); the last argument tells the solver we want
25 topics in our topic model.

3. Solving NMF
--------------

Since `my_input` contains (nearly) all the information the solver needs,
we now simply put

    my_output = solve_nmf(my_input, project = FALSE)

And there you have it. `my_output` contains *Φ* (the word-topic matrix),
*Θ* (the topic-document matrix), the anchor words, and some relevant
infomration from `my_input` that the inference functions will need to do
their thing:

    str(my_output)

    ## List of 6
    ##  $ phi    : num [1:2997, 1:25] 0.723 0 0 0 0 ...
    ##  $ theta  : num [1:25, 1:788] 0 0 0 0 2.21 ...
    ##  $ anchors: chr [1:25] "banished" "child" "day" "dead" ...
    ##  $ vocab  : chr [1:2997] "banished" "child" "day" "dead" ...
    ##  $ groups : Factor w/ 5 levels "five","four",..: 3 3 3 3 3 3 3 3 3 3 ...
    ##  $ topics : int 25
    ##  - attr(*, "class")= chr "nmf_output"

If you’re just interested in the usual topic modelling pipeline, then
you’re done at this stage. Print the top words for each topic using

    print_top_words(my_output, n = 10)

    ## $banished
    ##  [1] "banished" "word"     "woe"      "lives"    "husband"  "kill"    
    ##  [7] "mother"   "flies"    "sin"      "father"  
    ## 
    ## $child
    ##  [1] "child"         "murther"       "soul"          "joys"         
    ##  [5] "cam"           "despis"        "distressed"    "hated"        
    ##  [9] "myr"           "uncomfortable"
    ## 
    ## $day
    ##  [1] "day"        "woful"      "lamentable" "black"      "hateful"   
    ##  [6] "behold"     "woe"        "days"       "lark"       "sun"       
    ## 
    ## $dead
    ##  [1] "dead"    "kill"    "alack"   "husband" "undone"  "bid"     "watch"  
    ##  [8] "paris"   "woe"     "county" 
    ## 
    ## $death
    ##  [1] "death"      "lips"       "life"       "banishment" "breath"    
    ##  [6] "beauty"     "vault"      "lie"        "grave"      "fear"      
    ## 
    ## $fair
    ##  [1] "fair"     "beauty"   "passing"  "feast"    "precious" "married" 
    ##  [7] "book"     "paris"    "forget"   "read"    
    ## 
    ## $god
    ##  [1] "god"       "dug"       "fool"      "pretty"    "quoth"     "soul"     
    ##  [7] "dovehouse" "eve"       "lammas"    "susan"    
    ## 
    ## $heaven
    ##  [1] "heaven"  "married" "lives"   "tears"   "advanc"  "maid"    "nature" 
    ##  [8] "comfort" "array"   "life"   
    ## 
    ## $juliet
    ##  [1] "juliet" "paris"  "county" "beauty" "grave"  "talk"   "kiss"   "tomb"  
    ##  [9] "breath" "hand"  
    ## 
    ## $lady
    ##  [1] "lady"     "madam"    "mistress" "alas"     "warrant"  "wake"    
    ##  [7] "rest"     "county"   "fie"      "marry"   
    ## 
    ## $light
    ##  [1] "light" "dark"  "lark"  "fain"  "true"  "chang" "hes"   "prove" "quiet"
    ## [10] "arm"  
    ## 
    ## $lord
    ##  [1] "lord"    "paris"   "joyful"  "toad"    "friar"   "news"    "father" 
    ##  [8] "warrant" "husband" "lay"    
    ## 
    ## $love
    ##  [1] "love"  "hate"  "shape" "heavy" "die"   "eyes"  "happy" "thine" "wit"  
    ## [10] "birth"
    ## 
    ## $montague
    ##  [1] "montague" "red"      "hear"     "wife"     "true"     "prince"  
    ##  [7] "disturb"  "streets"  "cank"     "pain"    
    ## 
    ## $night
    ##  [1] "night"   "black"   "lie"     "true"    "stars"   "dream"   "wagoner"
    ##  [8] "whip"    "gentle"  "fiery"  
    ## 
    ## $nurse
    ##  [1] "nurse"   "faith"   "hear"    "morrow"  "shalt"   "music"   "comfort"
    ##  [8] "husband" "bed"     "alack"  
    ## 
    ## $peace
    ##  [1] "peace"   "talk"    "cank"    "pain"    "disturb" "streets" "hate"   
    ##  [8] "hands"   "capulet" "dep"    
    ## 
    ## $romeo
    ##  [1] "romeo"    "friar"    "bed"      "cold"     "body"     "die"     
    ##  [7] "wake"     "marriage" "dagger"   "tomb"    
    ## 
    ## $sir
    ##  [1] "sir"     "bite"    "thumb"   "paris"   "hare"    "bid"     "lick"   
    ##  [8] "weeps"   "fingers" "quarrel"
    ## 
    ## $speak
    ##  [1] "speak"    "word"     "true"     "hear"     "conjure"  "knife"   
    ##  [7] "canst"    "mightst"  "likeness" "fain"    
    ## 
    ## $stand
    ##  [1] "stand"      "weeping"    "sake"       "fall"       "lies"      
    ##  [6] "blubb"      "blubbering" "rise"       "deep"       "hear"      
    ## 
    ## $sweet
    ##  [1] "sweet"     "meet"      "nightly"   "news"      "sleep"     "bid"      
    ##  [7] "obsequies" "strew"     "joy"       "boy"      
    ## 
    ## $time
    ##  [1] "time"     "marriage" "hour"     "letter"   "potion"   "untimely"
    ##  [7] "cell"     "bear"     "writ"     "wild"    
    ## 
    ## $tybalt
    ##  [1] "tybalt"   "slain"    "mercutio" "husband"  "woe"      "cousin"  
    ##  [7] "friends"  "mother"   "father"   "prince"  
    ## 
    ## $wilt
    ##  [1] "wilt"    "fall"    "quarrel" "quoth"   "dost"    "jule"    "eye"    
    ##  [8] "dug"     "pretty"  "egg"

where `n=10` says we’d like 10 words printed per topic. Topics are
labeled by their anchor word.

Now, a word on the argument `solve_nmf(..., project = FALSE)`. This
tells the solver that we do *not* want to perform random projections
prior to solving NMF. Random projections send the data down to a random
low-dimensional subspace, perform the linear algebra calculations there,
and then map back up to the original space. Used appropriately, these
can get provably close to the original solution while providing
potentially large speed-ups. The TDM is small enough here where this
isn’t really an issue, and in fact such a projection may even be
detrimental since “small” data is subject to higher distorition. But,
for large matrices this is a really nice option; we’d specify something
like

    my_output = solve_nmf(my_input, project = TRUE, proj_dim = c(200, 200))

which tells the solver we’d like to project down to 200 dimensions to
find the anchor words and likewise for solving the non-negative least
squares problems that constitute convex NMF. It’s up to you to get these
dimensions right – that is, ensuring that they are actually smaller than
the input dimension (which is the number of columns in the TDM). For
some context, we’ve solved NMF for matrices of size ~ 10,000 x 6,000 in
a matter of minutes with very good results. Again, this is down to the
data as well – the more signal your TDM carries, the better these
projections will work.

4. Inferece I. Permutation Tests
--------------------------------

Now for inference. While you can certainly use this package for
straight-up topic modelling, or NMF more generally, a nice feature is
that we can do inference on the document groups. Recall that these are
the play’s acts. Basically, we’re interested in the relationship between
topics and groups. We’ll answer this question two ways:

1.  Using permutation tests to assess whether the topic structure is
    equal among the groups.
2.  Using the bootstrap to estimate the association between a given
    topic and each group.

The first produces output that is simpler to digest; let’s begin there
(please see the thesis linked above for the rationale behind these
tests). We run:

    perm_test(my_output, all = TRUE, permutations = 1000)

    ## observed test statistics calculated -- performing test
    ## test 1 of 10 complete
    ## test 2 of 10 complete
    ## test 3 of 10 complete
    ## test 4 of 10 complete
    ## test 5 of 10 complete
    ## test 6 of 10 complete
    ## test 7 of 10 complete
    ## test 8 of 10 complete
    ## test 9 of 10 complete
    ## test 10 of 10 complete

    ##                Observed Cos. Dist. p-val
    ## five vs. four            0.6972272 0.002
    ## five vs. one             0.5627826 0.000
    ## four vs. one             0.6292156 0.000
    ## five vs. three           0.7337528 0.000
    ## four vs. three           0.7154594 0.000
    ## one vs. three            0.5687604 0.000
    ## five vs. two             0.6000856 0.000
    ## four vs. two             0.6238488 0.000
    ## one vs. two              0.8404043 0.004
    ## three vs. two            0.7197113 0.000

What we get out is a list of all pairwise tests, the observed test
statistics, and a *p*-value. The fact that we specified
`perm_test(..., all = TRUE, ...)` meant that all possible pairwise tests
were performed; we could have instead said
`perm_test(..., all = FALSE, groups = c("one", "two", "three"), ...)`
which would instead perform all 3!/2! = 6 tests between acts one, two,
and three. Note *p*=0 should really be interpreted as *p* = *ϵ*, as
these are Monte Carlo permutation tests. So, what have we learned? Well,
that the acts of RJ are “different.” *Duh*. This is a case of using
statistics when you already know the answer, but it illustrates the
point. A much more realistic application might attempt to detect
statistically significant differences in content from different news
sources, for instance.

5. Inference II. The Bootstrap
------------------------------

So, permutation tests have told you that the topic structure between a
set of document groups is different. Great, but that’s not entirely
useful by itself. Surely you’d like to know *how* this topic structure
differs. In other words, we’d like some estimate of the association
between topics and groups. For this, we need two tools: Sinkhorn-Knopp
matrix balancing, and the non-parametric bootstrap. I’ll assume you’re
familiar with the boostrap, so what is Sinkhorn-Knopp? Basically, it’s a
way of stripping out the essential relationship between a matrix’s rows
and columns by controlling for unequal row and column variation. We run
such an algorith on *Θ̂*, the group-topic matrix (see thesis for
details), to give an output matrix *Θ̃* whose (*i,j*)th entry is a
measure of association between group topic *i* and group *j*. Things are
scaled so that “1” means neutral – no association – while minimal and
maximal associations are acheived at 0 and *G* respectively, *G* being
the number of groups.

We now pass topics (indicated by anchor words) we’re interested in to
the `booted(...)` function like so:

    #booted(my_output, all = FALSE, topics = c("love", "night"), alpha = .05, samples = 100)
    # This code chunk won't work in Rmarkdown. Hmm. The function works ,though!

This returns bootstrap confidence intervals (with the tail probabilities
controlled by *α*) and the point estimate of association. Because our
topics of interest are `love` and `night`, we got back a list with two
elements; `booted(..., all = TRUE, ...)` functions similarly to the
permutation test case. These are most associated with act II.

Perhaps you’re just interested in point estimates, and you’d like the
entire matrix of topic-group associations. This is gotten with

    run_sk(my_output, tolerance = 1e-5, maxiter = 100)

    ##               five      four        one     three        two
    ## banished 0.0000000 0.0000000 0.00000000 2.2360420 0.00000000
    ## child    0.0000000 2.0672580 0.45604623 0.7200446 0.00000000
    ## day      0.2446524 1.9235903 0.53602419 0.8933384 0.39314462
    ## dead     1.7650687 1.1869280 0.16113326 0.6360273 0.21272848
    ## death    1.7005399 1.1393466 0.29569986 0.8092519 0.26025574
    ## fair     0.3750458 0.3104014 2.09992805 0.1922055 0.56250166
    ## god      0.0000000 0.8987531 1.26892091 1.0017401 1.25642412
    ## heaven   1.1327838 1.1719159 0.20682384 1.4150547 0.54609858
    ## juliet   2.0482534 0.5353290 0.47238370 0.5386619 0.06929356
    ## lady     0.8716658 1.2023701 1.16709180 0.6142346 1.02719810
    ## light    1.0644589 0.2202461 1.55479264 0.8182796 0.85526694
    ## lord     1.2071706 0.9990979 0.35264864 1.1135833 1.08632418
    ## love     0.4903552 0.4796240 1.33479807 0.5140226 1.57595421
    ## montague 1.0530447 0.0000000 1.84574477 0.1619010 0.67687675
    ## night    0.6705288 0.9018000 0.97940209 0.7087499 1.50851025
    ## nurse    0.1473852 1.5857571 0.86110769 0.4531967 1.23157261
    ## romeo    1.2420004 0.4205145 0.37106940 1.2368492 1.27007647
    ## sir      0.6124109 1.1150774 1.52067157 0.5649331 0.86602092
    ## speak    0.0000000 0.8107526 0.35771117 1.3178313 1.57417023
    ## stand    1.2709587 0.2103783 1.29949038 0.5862131 1.14372670
    ## stay     1.8221937 0.3351363 0.59146031 0.6225652 0.91098839
    ## sweet    0.5236249 0.1444569 0.63735676 0.4696128 2.01945551
    ## time     0.9076521 1.8028935 0.79545251 0.4884165 0.23336848
    ## tybalt   0.3582783 0.3953654 0.08721934 2.1574460 0.23029434
    ## wilt     0.2512115 0.2079117 1.28425467 1.2552370 1.29179111

Those last two arguments just control convergence, and make sure the
algorithm doesn’t run forever. It’s nice to sort it, such as with

    rj_mat = run_sk(my_output)
    sort(rj_mat[,"five"], decreasing = TRUE)

    ##    juliet      stay      dead     death     stand     romeo      lord    heaven 
    ## 2.0482534 1.8221937 1.7650687 1.7005399 1.2709587 1.2420004 1.2071706 1.1327838 
    ##     light  montague      time      lady     night       sir     sweet      love 
    ## 1.0644589 1.0530447 0.9076521 0.8716658 0.6705288 0.6124109 0.5236249 0.4903552 
    ##      fair    tybalt      wilt       day     nurse  banished     child       god 
    ## 0.3750458 0.3582783 0.2512115 0.2446524 0.1473852 0.0000000 0.0000000 0.0000000 
    ##     speak 
    ## 0.0000000

So `juliet`, `death`, and `dead` are highly associated with act V. Go
figure.

The End
-------

Cool. Hope that gives you a feel for the package. Happy NMF’ing!
