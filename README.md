## inferNMF 

This is a minimal package to perfrom convex non-negative matrix factorization 
with a dollop of non-parametric statistical inference. Anchor words are used 
to "convexify" the NMF problem, while we infer the properties of potential 
group structure on the documents using permutation tests and the bootstrap. 

Please see [link to thesis when available] for further motivation, mathematical details, and references to the requisite literature. 

# Installation 

1. Clone this repository. 
2. Navigate to it using the shell. 
3. Fire up R (make sure you're in the inferNMF directory). 
4. Run the following: 
	``` 
	library(devtools) ##install if need be 
	build() 
	install() 
	``` 
5. That's it. You should now be able to load inferNMF like any other
package. 

# Warning 

This package is in its early stages. Stuff is bound to not work under weird-ish inputs.  
